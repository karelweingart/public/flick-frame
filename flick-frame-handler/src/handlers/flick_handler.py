import flicklib, asyncio
from handlers.pyppeteer_handler import _method_mapping
from threading import Lock
from enums.enums import FlickTypes
from handlers.action_map import ActionMap
from typing import Any

class SharedObjectContainer():
    lock: Lock
    loop: asyncio.AbstractEventLoop


    def startFlick():
        while True:
            pass

def process_action(type: FlickTypes):
    _pyppe_packet = ActionMap.get(type.value)
    print(_pyppe_packet)
    if _pyppe_packet:
        SharedObjectContainer.loop.run_until_complete(_method_mapping.get(_pyppe_packet.action_type)(_pyppe_packet.action_data))
    print(type)


#@flicklib.move()
def _move(x: float, y: float, z: float) -> bool:
    with SharedObjectContainer.lock:
        process_action(FlickTypes.FLICK_MOVE)

@flicklib.flick()
def _flick(start: str, finish: str) -> bool:
    with SharedObjectContainer.lock:
        if start == "west" and finish == "east":
            process_action(FlickTypes.FLICK_TO_LEFT)
        if start == "east" and finish == "west":
            process_action(FlickTypes.FLICK_TO_RIGHT)
        if start == "north" and finish == "south":
            process_action(FlickTypes.FLICK_DOWN)
        if start == "south" and finish == "north":
            process_action(FlickTypes.FLICK_UP)


