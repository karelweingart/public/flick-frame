import os, logging, json
from pyppeteer import launch
from pyppeteer.browser import Browser
from typing import Any, Coroutine, Callable
from threading import Lock
from functools import wraps
from enums.enums import PyppeTargetTypes
from handlers.action_map import ActionMap

logging.basicConfig(level=logging.INFO)

#load page; get config by id element id(element id is startup param  and the same for all pages)
#parse the
_method_mapping: dict = {}

class PyppeHandler:

    _browser: Coroutine[Any, Any, Browser]
    _page: Any
    

    def processor_method(**kwargs):
        """Decorator method"""
        def inner(func):
            global _method_mapping
            @wraps(func)
            def wrapped_f(*args):
                return func(*args)

            _method_mapping[kwargs['pyppe_action']] = wrapped_f
            return wrapped_f
            logging.info(_method_mapping)
        return inner

    @staticmethod
    async def start_session(url: str, executablePath: str, lock: Lock) -> None:
        with lock:
            if not os.getenv('DISPLAY'):
                logging.info("set DISPLAY envvar")
                os.environ['DISPLAY'] = ':0'
            PyppeHandler._browser = await launch(headless=False, 
                                                     executablePath=executablePath, 
                                                     args=['--start-maximized', '--start-fullscreen'],
                                                     ignoreDefaultArgs=['--enable-automation'])
            PyppeHandler._page = await PyppeHandler._browser.newPage()
            await PyppeHandler._goto(url)

    @staticmethod
    def set_method(pyppeAction: str, target_function: Callable) -> None:
        PyppeHandler._method_mapping[pyppeAction] = target_function
    
    @staticmethod
    @processor_method(pyppe_action=PyppeTargetTypes.PYPPE_GOTO.value)
    async def _goto(action_data: str) -> None:
       await PyppeHandler._page.goto(action_data)
       PyppeHandler._page.on("response", await PyppeHandler._evaluate("flick_mapping"))

    @staticmethod
    @processor_method(pyppe_action=PyppeTargetTypes.PYPPE_CLOSE.value)
    async def _close(action_data: str) -> None:
        await PyppeHandler._browser.close()

    @staticmethod
    @processor_method(pyppe_action=PyppeTargetTypes.PYPPE_EVALUATE.value)
    async def _evaluate(action_data: str) -> None:
        data = await PyppeHandler._page.evaluate(action_data)
        #_dict =json.loads(data)
        for k, v in data.items():
            ActionMap.put(k, v)


                 