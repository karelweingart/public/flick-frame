from enum import Enum

class FlickTypes(Enum):

    FLICK_MOVE = "flicklib.move"
    FLICK_FLICK = "flicklib.flick"
    FLICK_TO_LEFT = "flicklib.flick.toleft"
    FLICK_TO_RIGHT = "flicklib.flick.toright"
    FLICK_UP = "flicklib.flick.up"
    FLICK_DOWN = "flicklib.flick.down"


class PyppeTargetTypes(Enum):
    PYPPE_GOTO = "pyppe.goto"
    PYPPE_CLOSE = "pyppe.close"
    PYPPE_EVALUATE = "pyppe.evaluate"