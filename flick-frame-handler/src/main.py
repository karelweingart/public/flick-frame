from  threading import Lock
from handlers.pyppeteer_handler import PyppeHandler, _method_mapping
import asyncio
from enums.enums import PyppeTargetTypes
from handlers.action_map import ActionItem, ActionMap
from handlers.flick_handler import SharedObjectContainer
import os


ActionMap._web_local_root = os.getenv("WEB_LOCAL_ROOT")
sync_lock = Lock()
SharedObjectContainer.lock = sync_lock

asyncio.get_event_loop().run_until_complete(PyppeHandler.start_session(url=f"{ActionMap._web_local_root}/clocks.html",
                                                                       executablePath='/usr/bin/chromium-browser',
                                                                       lock=sync_lock))
SharedObjectContainer.loop =  asyncio.get_event_loop()
SharedObjectContainer.startFlick()